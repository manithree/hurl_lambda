# hurl_lambda

## What is hurl_lambda?
hurl_lambda is small wrapper around the [hurl crate](https://hurl.dev/) to allow easy authoring of monitoring (AWS) lambdas using hurl.

My intention was to make availability monitors that are easier to author and maintain, and more efficient than python or javascript scripts.

## How to use it

Simply use the hurl cli to develop a .hurl file that tests or checks your web site or service. Name that lambda.hurl and put it in a zip file with this executable. Upload that as a lambda, set up an EventBridge event, and you have a monitor running and reporting to CloudWatch Metrics.

The minimal event required is:

``` json
{
  "monitor_name": "AvailabilityMonitor1",
  "metric_namespace": "my_monitors"
}
```

You can add variables, and encrypted variables to use in hurl, also:

``` json
{
  "monitor_name": "AvailabilityMonitor1",
  "metric_namespace": "my_monitors",
  "kms_keyid": "arn:aws:kms:us-worst-2:**************",
  "variables": [
    {
      "name": "base_url",
      "value": "http://test.io/api/v2"
    }
  ],
  "encrypted_variables": [
    {
      "name": "form_user",
      "value": "*************"
    },
    {
      "name": "form_passwd",
      "value": "***********"
    }
  ]
}
```

The variables need to be encrypted just like the environment variable helper in the console does it, with the LambdaFunctionName in the encryption context.

Result metrics (0-success) and elapsed time metrics will be written to CloudWatch Metrics.

## How to build it

build_docker/Dockerfile contains the docker that I use to build the lambda executable. From inside that docker, or on an Amazon Linux 2023 ec2 instance, just 'cargo-build --release'

