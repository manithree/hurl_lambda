use base64::{engine::general_purpose, Engine as _};
use std::env;
use std::fs;

use hurl::runner;
use hurl::runner::{RunnerOptionsBuilder, Value};
use hurl::util::logger::{LoggerOptionsBuilder, Verbosity};

use aws_sdk_cloudwatch::types::{Dimension, MetricDatum, StandardUnit};
use aws_sdk_kms::primitives::Blob;
use aws_sdk_s3::Client as S3Client;
use lambda_runtime::{service_fn, Error, LambdaEvent};

use serde::{Deserialize, Serialize};

const STATIC_HURL_FILE: &str = "lambda.hurl";

#[derive(Deserialize)]
struct HurlVar {
    name: String,
    value: String,
}

#[derive(Deserialize)]
struct HurlLambdaEvent {
    monitor_name: String,
    metric_namespace: String,
    kms_keyid: Option<String>,
    s3_bucket: Option<String>,
    s3_key: Option<String>,
    verbose: Option<bool>,
//    insecure: Option<bool>,
    variables: Option<Vec<HurlVar>>,
    encrypted_variables: Option<Vec<HurlVar>>,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(hurl_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

pub(crate) async fn hurl_handler(event: LambdaEvent<HurlLambdaEvent>) -> Result<Response, Error> {
    let config = aws_config::load_defaults(aws_config::BehaviorVersion::latest()).await;
    let cw = aws_sdk_cloudwatch::Client::new(&config);

    let rq_id = event.context.request_id.clone();

    // set variables
    let mut variables = hurl::runner::VariableSet::new();
    match event.payload.variables {
        Some(var_vec) => {
            for var in var_vec {
                let _ = variables.insert(var.name, Value::String(var.value));
            }
        }
        None => println!("No variables"),
    }

    match event.payload.encrypted_variables {
        Some(evars) => {
            let kms = aws_sdk_kms::Client::new(&config);
            let lambda_name = env::var("AWS_LAMBDA_FUNCTION_NAME").unwrap();
            println!("Lambda name:{}", &lambda_name);
            let key_id = event.payload.kms_keyid.unwrap();
            for evar in evars {
                //decrypt value
                let value = Blob::new(general_purpose::STANDARD.decode(evar.value).unwrap());
                let decrypt_resp = kms
                    .decrypt()
                    .ciphertext_blob(value)
                    .key_id(&key_id)
                    .encryption_context("LambdaFunctionName", &lambda_name)
                    .send()
                    .await;
                match decrypt_resp {
                    Err(x) => println!("Error {:?}", x),
                    Ok(value) => {
                        let cont = value.plaintext.unwrap();
                        let bytes = cont.as_ref();
                        let _ =variables.insert(
                            evar.name,
                            Value::String(
                                String::from_utf8(bytes.to_vec())
                                    .expect("Could not convert to UTF-8"),
                            ),
                        );
                    }
                }
            }
        }
        None => println!("No encrypted variables"),
    }

    let mut s3_file: bool = false;
    let monitor_name = event.payload.monitor_name;
    let s3_local_file: String = format!("/tmp/{}.hurl", &monitor_name);

    // is the hurl file in s3?
    match event.payload.s3_bucket {
        Some(s3_bucket) => {
            if !std::path::Path::new(&s3_local_file).exists() {
                // download the .hurl file from s3
                let s3_key = event.payload.s3_key.unwrap();
                println!("Downloading {}/{}", s3_bucket, s3_key);
                let s3_client = S3Client::new(&config);
                let get_obj_resp = s3_client
                    .get_object()
                    .bucket(s3_bucket)
                    .key(s3_key)
                    .send()
                    .await;
                match get_obj_resp {
                    Err(x) => println!("GetObject Eror {:?}", x),
                    Ok(value) => {
                        let mut read = value.body.into_async_read();
                        let mut write = tokio::fs::File::create(&s3_local_file).await?;
                        tokio::io::copy(&mut read, &mut write).await?;
                        s3_file = true;
                    }
                }
            } else {
                println!("{} already downloaded", &s3_local_file);
                s3_file = true;
            }
        }
        None => println!("Not getting hurl file from s3 bucket"),
    }

    // Parse Hurl file
    let filename = if s3_file {
        &s3_local_file
    } else {
        STATIC_HURL_FILE
    };
    println!("Reading {}", filename);
    let s = fs::read_to_string(filename)?;

    let mut builder = LoggerOptionsBuilder::new();
    match event.payload.verbose {
        Some(_x) => builder.verbosity(Some(Verbosity::VeryVerbose)),
        None => &mut builder,
    };
    let logger = builder.build();

    // Define runner options
    let mut bldr = RunnerOptionsBuilder::new();
    bldr.follow_location(true);

    let runner_options = bldr.build();

    // Run the hurl file
    let hurl_results = runner::run(
        &s,
        Some(&hurl_core::input::Input::new(filename)),
        &runner_options,
        &variables,
        &logger,
    ).unwrap();

    let elapsed: f64 = hurl_results.duration.as_millis() as f64 / 1000.0_f64;
    println!("result: {}: {}", hurl_results.success, elapsed);
    let metric_namespace = event.payload.metric_namespace;
    // create the cw metric vector
    let mut metrics = Vec::<MetricDatum>::new();
    // timing
    metrics.push(
        MetricDatum::builder()
            .metric_name("elapsed")
            .value(elapsed)
            .unit(StandardUnit::Seconds)
            .dimensions(
                Dimension::builder()
                    .name("MonitorName")
                    .value(&monitor_name)
                    .build(),
            )
            .build(),
    );
    // status (for alarm)
    metrics.push(
        MetricDatum::builder()
            .metric_name("status")
            .value(if hurl_results.success {
                0.0_f64
            } else {
                2.0_f64
            })
            .unit(StandardUnit::None)
            .dimensions(
                Dimension::builder()
                    .name("MonitorName")
                    .value(&monitor_name)
                    .build(),
            )
            .build(),
    );
    // log the CloudWatch metrics
    let _resp = cw
        .put_metric_data()
        .namespace(metric_namespace)
        .set_metric_data(Some(metrics))
        .send()
        .await?;

    let resp = Response {
        req_id: rq_id,
        msg: format!("{} hurl check executed.", monitor_name),
    };
    Ok(resp)
}
